from django.conf.urls import include, url
from django.contrib import admin
from registration.backends.simple.views import RegistrationView
from django.conf import settings
from django.conf.urls.static import static


# Create a new class that redirects the user to the index page, if successful at logging
class MyRegistrationView(RegistrationView):
    def get_success_url(self,request, user):
        return '/blog/'

urlpatterns = [
    url(r'^polls/', include('polls.urls',namespace="polls")),
    url(r'^blog/', include('core.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/register/$', MyRegistrationView.as_view(), name='registration_register'),
    url(r'^accounts/', include('registration.backends.simple.urls')),
    url(r'^ckeditor/', include('ckeditor.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_URL, show_indexes=True )


