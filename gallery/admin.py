from django.contrib import admin

from .models import Image

class ImageAdmin(admin.ModelAdmin):
    search_fields = ["title"]

admin.site.register(Image, ImageAdmin)
