__author__ = 'n0npax'
from django.conf.urls import include, url
from . import views

urlpatterns = [
    url(r'^$', views.post_list, name='index'),
    url(r'^(?P<page_num>[0-9]+)/$', views.post_list, name='list'),
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name='post'),
    url(r'^post/new/$', views.post_new, name='post_new'),
    url(r'^post/(?P<pk>[0-9]+)/edit/$', views.post_edit, name='post_edit'),
    url(r'^post/(?P<pk>[0-9]+)/del/$',  views.post_del,  name='post_del'),
    url(r'^gallery/', include('photologue.urls', namespace='photologue'), name='photologue'),

]