from django.shortcuts import render
from django.utils import timezone
from .models import Post
from .forms import PostForm
from django.shortcuts import render, get_object_or_404
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required

def post_list(request, page_num=None,posts_per_page=5):
    if page_num==None: return redirect('core.views.post_list', page_num=0)
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    page_num = int(page_num)
    max_page_num = int(len(posts)/posts_per_page+1)
    posts = posts[page_num*posts_per_page:page_num*posts_per_page+posts_per_page]
    return render(request, 'blog/post_list.html', {'posts': posts, 'act_page_num' : page_num, 'is_last_page' : not bool(max_page_num-page_num-1) , 'pages_numbers':range(max_page_num)})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})

@login_required
def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            print post.text
            post.publish()
            post.save()
            post.save()
            return redirect('core.views.post_detail', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'blog/post_edit.html', {'form': form})

@login_required
def post_del(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == 'POST':
        post.delete()
        return redirect('core.views.post_list')
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_del.html', {'form': form})

@login_required
def post_edit(request, pk):
    post = get_object_or_404(Post, pk=pk)
    if request.method == "POST":
        form = PostForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.author = request.user
            post.save()
            return redirect('core.views.post_detail', pk=post.pk)
    else:
        form = PostForm(instance=post)
    return render(request, 'blog/post_edit.html', {'form': form})